from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty, StringProperty, ListProperty
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout

from kivymd.list import MDList, OneLineListItem, OneLineAvatarIconListItem
from kivymd.button import MDFlatButton
from kivymd.ripplebehavior import RectangularRippleBehavior
from kivymd.label import MDLabel

from modals import GameModal, EditRoundModal



class GameRounds(GridLayout):

    rounds = ListProperty()

    def __init__(self, rounds=[], **kwargs):
        super(GameRounds, self).__init__(**kwargs)

        self.cols = 1
        self.row_default_height = '30dp'
        self.row_force_default = True
        self.size_hint_y = None
        self.height = self.minimum_height
        self.rounds = rounds 

    def on_rounds(self, *args):
        print("on_rounds: start")
        
        self.clear_widgets()

        for gRound in self.rounds:  
            print("on_rounds: forLoop")
            print(gRound)
            print(self)
        
            self.add_widget(gRound)

        print("on_rounds: end")

class gRoundWidget(BoxLayout, RectangularRippleBehavior):

    items = ListProperty()
    roundNum = NumericProperty()
    ModalView= ObjectProperty()

    def __init__(self, GameRoundItems=[], **kwargs):
        super(gRoundWidget, self).__init__(**kwargs)

        self.orientation = "horizontal"
        self.items = GameRoundItems
        self.ModalView=EditRoundModal(self)

    def on_items(self, *args):

        print("on_items: start")

        self.clear_widgets()

        for item in self.items:
            print(item)
            self.add_widget(item)

    def open_modal(self):
        
        self.ModalView.open()  

        pass

class GameRoundItem(MDLabel):

    roundNum=NumericProperty()
   
    def __init__(self, **kwargs):
        super(GameRoundItem, self).__init__(**kwargs)


        


    
    pass
