from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty, StringProperty
from kivy.vector import Vector
from kivy.clock import Clock
from random import randint
from kivy.uix.button import Label
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.graphics import Color
from kivy.uix.popup import Popup
from kivy.core.window import Window
from kivy.storage.jsonstore import JsonStore
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.modalview import ModalView
from kivy.animation import Animation
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout

from kivymd import snackbar as Snackbar
from kivymd.theming import ThemeManager
from kivymd.label import MDLabel
from kivymd.button import MDIconButton, MDRaisedButton, MDFlatButton
from kivymd.list import MDList, OneLineListItem, OneLineAvatarIconListItem
from kivymd.theming import ThemableBehavior
from kivymd.backgroundcolorbehavior import SpecificBackgroundColorBehavior
from kivymd.elevationbehavior import RectangularElevationBehavior
from kivymd.navigationdrawer import NavigationDrawer
from kivymd.list import ILeftBody, ILeftBodyTouch, IRightBodyTouch
from kivymd.selectioncontrols import MDCheckbox
from kivymd.card import MDCard
from kivymd.ripplebehavior import CircularRippleBehavior

from modals import GameModal
from gameRound import GameRoundItem, GameRounds, gRoundWidget



#from kivymd.button import MDRaisedButton as Button

class InputModal(GameModal):

    amount = NumericProperty()

    def __init__(self, target,**kwargs):
        super(InputModal,self).__init__(**kwargs)

        self.target = target
        self.amount = target.value
        # print(target.value)
        #self.ids.lblInputAmount.text = str(self.amount.value)

    def on_open(self):
        
        self.amount = self.target.value

        
    def add100(self):
        self.amount +=100
        
        pass

    def add10(self):
        self.amount +=10
        
        pass

    def minus100(self):
        self.amount -=100
        if self.amount < 0:
            self.amount = 0
        pass

    def minus10(self):
        self.amount -=10
        if self.amount < 0:
            self.amount = 0
        pass

    def save(self):
        self.target.value = self.amount
        app.sm.get_screen('Game').calc_tricks(self.target.teamNum)
        #print(self.target.teamNum)
        pass


    pass
class TrickModal(InputModal):

    def add100(self):
        self.amount +=100
        if self.amount >game.totalTricks:
            self.amount = game.totalTricks
        pass

    def add10(self):
        self.amount +=10
        if self.amount >game.totalTricks:
            self.amount = game.totalTricks
        pass

    pass


class CheckboxRight(IRightBodyTouch, MDCheckbox):
    pass

class Team(object):

    def __init__(self, name = 'Team', score =0):
        self.name= name
        self.score = score

    pass

class Game(object):

    def __init__(self, players=4):

        self.players = players
        self.rounds =[]


        if players == 6:
            self.totalTricks = 500
            self.initialBid = 300
        else:
            self.totalTricks = 250
            self.initialBid = 150

        pass

class GameRound(object):

    def __init__(self, bidTeam ='',bid = 150, teams=[], scores=[], roundNum=1):

        self.bidTeam = bidTeam
        self.teams = teams
        self.bid = bid 
        self.scores = scores
        self.RoundNum=roundNum


    pass




class InputButton(OneLineListItem):

    InputModal= ObjectProperty()
    value = NumericProperty()
    teamNum= NumericProperty()

    def __init__(self, teamNum=1,**kwargs):
        super(InputButton, self).__init__(**kwargs)


        self.InputModal=InputModal(self)
        self.value = 0
        self.teamNum = teamNum


    def on_press(self, *args):

        self.open_modal()

    def open_modal(self):
        
        self.InputModal.open()  

        pass

class TrickButton(InputButton):
    def __init__(self, teamNum=1,**kwargs):
        super(TrickButton, self).__init__(**kwargs)
        self.InputModal = TrickModal(self)
    pass

class MenuScreen(Screen):
    global TeamOne 
    global TeamTwo 

    TeamOne = Team('Team 1', 0)
    TeamTwo = Team('Team 2', 0)
    pass

class SettingsScreen(Screen):



    def on_enter(self):
        pass 
        
    pass



class TeamsScreen(Screen):
    


    def get_names(self):

        TeamOne.name = self.ids['txtTeamOneName'].text
        TeamTwo.name = self.ids['txtTeamTwoName'].text


        # print(self.ids.TeamOneName.text)
        # TeamOneName = self.ids.TeamOneName.text
        # TeamTwoName = self.ids.TeamTwoName.text
    pass

class GameScreen(Screen):



    # global lblTeam1
    # global lblTeam2
    # global lblTeamOneName
    # global lblTeamTwoName

    # def __init__(self, name):
    #     lblTeam1 = self.ids['lblTeam1'].text
    #     lblTeam2 = self.ids['lblTeam2'].text
    #     lblTeamOneName = self.ids['lblTeamOneName'].text
    #     lblTeamTwoName = self.ids['lblTeamTwoName'].text

    #     tglBidTeamOne = self.ids['tglBidTeamOne'].text
    #     tglBidTeamTwo = self.ids['tglBidTeamTwo'].text

    #     txtBidAmount = self.ids['txtBidAmount'].text
    #     btnInputMeld1 = self.ids['btnInputMeld1'].text
    #     btnInputMeld2 = self.ids['btnInputMeld2'].text
    #     btnInputTrick1 = self.ids['btnInputTrick1'].text
    #     btnInputTrick2 = self.ids['btnInputTrick2'].text
    logs = ObjectProperty(None)


    def on_enter(self): 

        
        global btnInputMeld2
        global btnInputMeld1
        global btnInputTrick2
        global btnInputTrick1

        global tglFourPlayer
        global tglSixPlayer

        global game
        global sldrBidAmount

        #print(TeamOne.name, TeamOne.scoreTeamTwo.name, TeamTwo.score)
        #self.ids['lblTeamOneName'].text = TeamOne.name
        #self.ids['lblTeamTwoName'].text = TeamTwo.name
        #self.ids['tglBidTeamOne'].text = TeamOne.name
        #self.ids['tglBidTeamTwo'].text = TeamTwo.name
        #self.ids['lblTeam1'].text = TeamOne.name
        #self.ids['lblTeam2'].text = TeamTwo.name
        self.ids['lblLogTeam1Score'].text = TeamOne.name
        self.ids['lblLogTeam2Score'].text = TeamTwo.name
        self.ids['lblTeam1Name'].text = TeamOne.name
        self.ids['lblTeam2Name'].text = TeamTwo.name

        btnInputMeld2 = self.ids['btnInputMeld2']
        btnInputTrick1 = self.ids['btnInputTrick1']
        btnInputMeld1 = self.ids['btnInputMeld1']
        btnInputTrick2 = self.ids['btnInputTrick2']

        sldrBidAmount = self.ids.sldrBidAmount


        
        #print('nav_drawer: '+str(app.nav_drawer._list))

        tglFourPlayer = app.sm.get_screen('Teams').ids.fourplayer
        tglSixPlayer = app.sm.get_screen('Teams').ids['sixplayer']

        #self.manager.get_screen('menu')

        players = 4

        if tglSixPlayer.state == 'down':
            players = 6
        else:
            players = 4



         

        game = Game(players)
        #print(game.players, game.totalTricks)


        

        self.initialize_values()

        

##############################################################
        ## FOR TESTING PURPOSES ONLY!##
##############################################################
        # self.ids['btnInputMeld1'].text = '0'
        # self.ids['btnInputMeld2'].text = '0'
        # self.ids['btnInputTrick1'].text = '0'
        # self.ids['btnInputTrick2'].text = '0'
        # self.ids['tglBidTeamOne'].state = 'down'

    def initialize_values(self):

        #self.ids['txtBidAmount'].text = str(game.initialBid)
# * I may need this in future...*

#         tricks =[]
#         for x in range(0, game.totalTricks+1,10):

#             tricks.append(str(x))
                
        

#         meld = []
#         for x in range(0,400):

#             meld.append(str(x*10))

#             #Spinner(text=str(x),values(str(x+10),str(x+20)))

        sldrBidAmount.min=game.initialBid

        btnInputMeld1.value =0
        btnInputMeld2.value =0
        btnInputTrick1.value =0
        btnInputTrick2.value =0



        # tricks = ''
        # meld = ''
        # btnInputMeld2.text = meld
        # btnInputTrick2.text = tricks
        
        # btnInputTrick1.text = tricks

        # btnInputTrick1.error = False
        # btnInputTrick1.message_mode = 'on_focus'
        # btnInputTrick1.focus = False

        # btnInputTrick2.error = False
        # btnInputTrick2.message_mode = 'on_focus'
        # btnInputTrick2.focus = False

        # # btnInputMeld1.error = False
        # # btnInputMeld1.message_mode = 'on_focus'
        # # btnInputMeld1.focus = False

        # btnInputMeld2.error = False
        # btnInputMeld2.message_mode = 'on_focus'
        # btnInputMeld2.focus = False

        # btnInputTrick1.hint_text='Tricks'
        # btnInputTrick2.hint_text='Tricks'

        pass

# ## still needing development....

#     def more_values(self, teamNum):
#         # global btnInputMeld1
#         # global btnInputMeld2
#         # global values
#         btnInputMeld2 = self.ids['btnInputMeld2']
       
#         btnInputMeld1 = self.ids['btnInputMeld1']

        
#         values =[]

#         if teamNum == 1:
#             btnInputMeld1.background_color = .15, .3, .79, 1

#         #     if str(btnInputMeld1.text)[-2:] =='00':

#         #         for x in range(0,11):
#         #             values.append(str(int(btnInputMeld1.text)+ (x*10)))

#         #         btnInputMeld1.values = values
                
#         else:
#             btnInputMeld2.background_color = .15, .3, .79, 1
#         #     if str(btnInputMeld2.text)[-2:] =='00':
#         #         for x in range(0,10):
#         #             values.append(str(int(btnInputMeld2.text)+ (x*10)))

#         #         btnInputMeld2.values = values
#         #         btnInputMeld2.is_open = True


        
        



    def calc_tricks(self, teamNum):

        btnInputTrick2 = self.ids['btnInputTrick2']
        
        btnInputTrick1 = self.ids['btnInputTrick1']
        

        ## this is to make sure that there actually is some text change in the spinners
        if btnInputTrick1.value =='' and btnInputTrick2.value =='':
            return

        if btnInputTrick1.value == None and btnInputTrick2.value ==None:
            return

        try:
            if teamNum == 1:

                btnInputTrick2.value =game.totalTricks - int(btnInputTrick1.value)
            
            else:
            
                btnInputTrick1.value =game.totalTricks - int(btnInputTrick2.value)
            
        except Exception, e:
            
            pass
        

        # btnInputTrick1.hint_text=''
        # btnInputTrick2.hint_text=''

    def submit(self):

        # first check that the amounts were put in
        tglBidTeamOne = self.ids['tglBidTeamOne']
        tglBidTeamTwo = self.ids['tglBidTeamTwo']
        #txtBidAmount = self.ids['txtBidAmount']
        sldrBidAmount = self.ids['sldrBidAmount']
        btnInputTrick1 =self.ids['btnInputTrick1']
        btnInputTrick2 =self.ids['btnInputTrick2']
        btnInputMeld1 =self.ids['btnInputMeld1']
        btnInputMeld2 =self.ids['btnInputMeld2']


        
        



        # escape = False
        #check bid team
        if tglBidTeamOne.state == 'normal' and tglBidTeamTwo.state == 'normal':
            Snackbar.make("Please select a bid team.")
            
            return None


        if btnInputTrick1.value ==0 and btnInputTrick2.value ==0:
            
            Snackbar.make("Please input tricks.")
            return None


        #     btnInputTrick1.error = True
        #     btnInputTrick1.message_mode = 'on_error'
        #     btnInputTrick1.focus = True


        #     ## need to set focus on error and reset to on_focus when initializing


        #     #btnInputTrick1.background_color = 255/255,127/255,73/255,1
        #     escape = True

        # if btnInputTrick2.text =='' :
        #     btnInputTrick2.error = True
        #     btnInputTrick2.message_mode = 'on_error'
        #     btnInputTrick2.focus = True
        #     escape = True

        # if btnInputMeld1.text =='':
        #     #btnInputMeld1.error = True
        #     #btnInputMeld1.message_mode = 'on_error'
        #     #btnInputMeld1.focus = True
        #     escape = True

        # if btnInputMeld2.text =='':
        #     btnInputMeld2.error = True
        #     btnInputMeld2.message_mode = 'on_error'
        #     btnInputMeld2.focus = True
        #     escape = True

        ## if any of these happened, exit function
        # if escape:
        #     return None
        ## logic here

        if tglBidTeamOne.state == 'down':
            bidTeam = TeamOne
        else:
            bidTeam = TeamTwo

        bid = int(sldrBidAmount.value)

        #calculate score

        RoundScore1 = int(btnInputMeld1.value) +int(btnInputTrick1.value)
        RoundScore2 = int(btnInputMeld2.value) +int(btnInputTrick2.value)
        
        #print(RoundScore1, RoundScore2)

        # you have to have tricks to score
        if btnInputTrick1.value == 0:
            RoundScore1 = 0

        if btnInputTrick2.value == 0:
            RoundScore2 = 0

        # did they get the bid? if not...

        if bidTeam == TeamOne:
            if RoundScore1 < bid:
                RoundScore1 = bid*-1
        else:
            if RoundScore2 < bid:
                RoundScore2 = bid*-1

        #print(RoundScore1, RoundScore2)

        scores = []

        scores.append(RoundScore1)
        scores.append(RoundScore2)

        #update scores
        self.update_score(RoundScore1,RoundScore2) 

        #Create a gameRound
        teams = [TeamOne, TeamTwo]

        gameRound = GameRound(bidTeam, bid, teams, scores)

        game.rounds.append(gameRound)
        # for gRound in game.rounds:

        #     print(gRound.scores)
        
        self.log_round(gameRound, len(game.rounds))
        #self.bind_rounds(game.rounds)

        #after finish clear form
        self.clear_form()

        pass



    def log_round(self, newRound, roundNum):

        logs = self.logs

        
        roundNumItem = GameRoundItem(id='roundNum'+str(roundNum), text= str(roundNum))
        bidTeam = GameRoundItem(id='bidTeam'+str(roundNum), text= str(newRound.bidTeam.name))
        bid = GameRoundItem(id='bid'+str(roundNum), text=str(newRound.bid) )
        score1 = GameRoundItem(id='score1'+str(roundNum),text= str(newRound.scores[0]))
        score2 = GameRoundItem(id='score2'+str(roundNum),text= str(newRound.scores[1]))
        
      

        # roundNumItem.bind(on_press=self.edit_round_modal)
        # bidTeam.bind(on_press=self.edit_round_modal)
        # bid.bind(on_press=self.edit_round_modal)
        # score1.bind(on_press=self.edit_round_modal)
        # score2.bind(on_press=self.edit_round_modal)
        
        

        items=[]

        items.append(roundNumItem)
        items.append(bidTeam)
        items.append(bid)
        items.append(score1)
        items.append(score2)

        


        gameRound = gRoundWidget(items)
        
       
        gameRound.bind(on_touch_down= self.edit_round_modal)

        logs.rounds.append(gameRound) 


        # list1 = self.ids['list1']
        # list2 = self.ids['list2']
        # list3 = self.ids['list3']
        # list4 = self.ids['list4']
        # list5 = self.ids['list5']
        

        # for child in [child for child in list1.children]:            
            
        #     list1.remove_widget(child) # this will delete all children

        # for child in [child for child in list2.children]:            
        #     list2.remove_widget(child) # this will delete all children

        # for child in [child for child in list3.children]:            
        #     list3.remove_widget(child) # this will delete all children

        # for child in [child for child in list4.children]:            
        #     list4.remove_widget(child) # this will delete all children

        # for child in [child for child in list5.children]:            
        #     list5.remove_widget(child) # this will delete all children


                # roundNum=len(gameRounds)
                # print('RoundNum: '+str(roundNum))



                # newRound = GameRound(id='gameRound'+str(roundNum))

                # newRound.add_widget(roundNum)
                # newRound.add_widget(bidTeam)
                # newRound.add_widget(bid)
                # newRound.add_widget(score1)
                # newRound.add_widget(score2)
                


                # gameRounds.add_widget(newRound)

        # for gameRound in reversed(gameRounds):

        #     print('index: '+str(gameRounds.index(gameRound)))

        #     roundList1=GameRoundItem(id='col1Round'+str(roundNum),roundNum=roundNum, font_style='Body1',text= str(roundNum), pos_hint= {'center_x': 0.8, 'center_y': 0.5})
        #     roundList1=GameRoundItem(id='col2Round'+str(roundNum),roundNum=roundNum, font_style='Body1',text = str(gameRound.bidTeam.name),pos_hint= {'center_x': 0.2, 'center_y': 0.5})
        #     roundList3=GameRoundItem(id='col3Round'+str(roundNum),roundNum=roundNum, font_style='Body1',text = str(gameRound.bid), pos_hint= {'center_x': 0.5, 'center_y': 0.5})
        #     roundList4=GameRoundItem(id='col4Round'+str(roundNum),roundNum=roundNum, font_style='Body1',text = str(gameRound.scores[0]), pos_hint= {'center_x': 0.5, 'center_y': 0.5})
        #     roundList5=GameRoundItem(id='col5Round'+str(roundNum),roundNum=roundNum, font_style='Body1',text = str(gameRound.scores[1]), pos_hint= {'center_x': 0.5, 'center_y': 0.5})
            
            
        #     roundList1.bind(on_release=self.edit_round_modal)
        #     #roundList2.bind(on_release=self.edit_round_modal)
        #     roundList3.bind(on_release=self.edit_round_modal)
        #     roundList4.bind(on_release=self.edit_round_modal)
        #     roundList5.bind(on_release=self.edit_round_modal)
            
            


        #     list1.add_widget(roundList1)
        #     #list2.add_widget(roundList2)
        #     list3.add_widget(roundList3)
        #     list4.add_widget(roundList4)
        #     list5.add_widget(roundList5)


           
        #     roundNum = roundNum-1
        # print('logging:')
        # for gameRound in reversed(gameRounds):
        #     roundList = GameRoundItem(text = str(gameRound.bidTeam.name))
        #     print("roundList: ",roundList)

    def edit_round_modal(self,gameRoundItem, *args):

        print("gameRoundItem.roundNum: "+str(gameRoundItem.roundNum))

        gameRoundItem.open_modal()


        pass

    def edit_round(self,gameRoundItem,score1=0, score2=0):

        if score1== '':
            score1 =0
        if score2== '':
            score2=0
                
        score1=int(score1)
        score2=int(score2)
        print("Score 1: "+str(score1))
        print("Score 2: "+str(score2))

        index = gameRoundItem.roundNum-1

        gRound = game.rounds[index]

        changeScore1 = score1-gRound.scores[0]
        changeScore2 = score2-gRound.scores[1]

        gRound.scores[0] = score1
        gRound.scores[1] = score2

        self.log_round(gRound, gameRoundItem.roundNum)
        
        self.update_score(changeScore1,changeScore2)

        pass

    def delete_round(self,gameRoundItem):


        index = gameRoundItem.roundNum-1
        print("index:" +str(index))
        gRound= game.rounds[index]

        
        score1= gRound.scores[0]*-1
        score2= gRound.scores[1]*-1

        self.update_score(score1, score2)

        del game.rounds[index]
        
        self.log_round(game.rounds)
        pass

    

    def update_score(self,score1, score2):

        TeamOne.score += score1
        TeamTwo.score += score2

        self.ids['lblScore1'].text = str(TeamOne.score)
        self.ids['lblScore2'].text = str(TeamTwo.score)


    def clear_form(self):

            self.initialize_values()
           
            self.ids['tglBidTeamOne'].state = 'normal'
            self.ids['tglBidTeamTwo'].state = 'normal'


            # btnInputTrick1.focus = True
            # btnInputTrick2.focus = True
            # btnInputMeld2.focus = True
            #btnInputMeld1.focus = True

##############################################################
        ## FOR TESTING PURPOSES ONLY!##
##############################################################
            # self.ids['btnInputMeld1'].text = '0'
            # self.ids['btnInputMeld2'].text = '0'
            # self.ids['btnInputTrick1'].text = '0'
            # self.ids['btnInputTrick2'].text = '0'
            # self.ids['tglBidTeamOne'].state = 'down'

    
            pass
        
 
    pass



class PinochleNavDrawer(NavigationDrawer):

    pass

class PinochleApp(App):


    theme_cls = ThemeManager()
    nav_drawer = ObjectProperty()

    Window.softinput_mode = 'below_target'

    sm = ScreenManager()

    RoundLogs = JsonStore('RoundLogs')




    def build(self):
        # Create the screen manager
        
        
        global app
        app = self

        # self.sm.add_widget(MenuScreen(name='menu'))
        self.sm.add_widget(TeamsScreen(name='Teams'))
        self.sm.add_widget(GameScreen (name='Game'))
        self.sm.add_widget(SettingsScreen(name='settings'))
        # self.sm.add_widget(FivePlayerScreen(name='fiveplayer'))
        # self.sm.add_widget(SixPlayerScreen(name='sixplayer'))
        # self.sm.add_widget(EightPlayerScreen(name='eightplayer'))

        self.nav_drawer= PinochleNavDrawer()

        return self.sm

    

if __name__ == '__main__':
    PinochleApp().run()


##pullTEST


######################################################
########### LIST OF EXTRA FEATURES TO ADD ############
######################################################

# saving game state
# getting rid of excess comments
# scroll for viewing logs
# error alert for selecting a bid Team
# night theming
# hard to press buttons
# processing 
# streamline adding rounds 
# default bid amount



########################################
########### FINISHED PARTS  ############
########################################

# edit function
# theming
# input modal
# must have tricks to score
# change number of players section

