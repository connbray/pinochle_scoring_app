from kivy.uix.modalview import ModalView
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty, StringProperty


from kivymd.theming import ThemableBehavior
from kivymd.backgroundcolorbehavior import SpecificBackgroundColorBehavior
from kivymd.elevationbehavior import RectangularElevationBehavior


class GameModal(ThemableBehavior, FloatLayout, ModalView,SpecificBackgroundColorBehavior,RectangularElevationBehavior):
    


    pass


class EditRoundModal(GameModal):

    #gameRoundItem = ObjectProperty()

    def __init__(self,gameRoundItem, **kwargs):
        super(EditRoundModal, self).__init__(**kwargs)
        
        self.gameRoundItem = gameRoundItem
        #self.gameScreen = gameScreen


    pass